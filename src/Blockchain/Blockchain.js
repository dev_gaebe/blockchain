"use strict";

const Block = require('./Block');
const Transaction = require('./Transaction');

class Blockchain{
  constructor() {
      this.chain = [this.createGenesisBlock()];
      this.difficulty = 2;
      this.pendingTransactions = [];
      this.miningReward = 1;
  }

  createGenesisBlock() {
      return new Block(Date.parse("2018-01-01"), [], "0");
  }

  getLatestBlock() {
      return this.chain[this.chain.length - 1];
  }

  getChain() {
      return this.chain;
  }

  minePendingTransactions(miningRewardAddress){
      const rewardTx = new Transaction(null, miningRewardAddress, this.miningReward, Date.now());
      this.pendingTransactions.push(rewardTx);

      let block = new Block(Date.now(), this.pendingTransactions, this.getLatestBlock().hash);
      block.mineBlock(this.difficulty);

      console.log('Block successfully mined!');
      this.chain.push(block);

      this.pendingTransactions = [];
  }

  createTransaction(transaction){
      this.pendingTransactions.push(transaction);
  }

  getBalanceOfAddress(address){
      let balance = 0;

      for(const block of this.chain){
          for(const trans of block.data){
              if(trans.fromAddress === address){
                  balance -= trans.amount;
              }

              if(trans.toAddress === address){
                  balance += trans.amount;
              }
          }
      }

      return balance;
  }

  //proof of work
  isChainValid() {
      for (let i = 1; i < this.chain.length; i++){
          const currentBlock = this.chain[i];
          const previousBlock = this.chain[i - 1];

          if (currentBlock.hash !== currentBlock.calculateHash()) {
              return false;
          }

          if (currentBlock.previousHash !== previousBlock.calculateHash()) {
              return false;
          }
      }

      return true;
  }
}

module.exports = Blockchain;
