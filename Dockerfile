FROM node:latest

# install Typescript compiler
RUN npm install -g typescript

RUN mkdir /home/blockchain

WORKDIR /home/blockchain
COPY src /home/blockchain/src
COPY package.json /home/blockchain
COPY tsconfig.json /home/blockchain

CMD ["npm install && npm run start"]