Damit das Projekt gestartet werden kann muss zuerst node installiert werden.

Dies kann unter der folgenden URL getätigt werden: https://nodejs.org/en/

Ist dies getan, so können über die Konsole nun folgende Befehle laufen gelassen werden:

npm install // damit die Packages installiert werden können,
npm run start // damit das Projekt gestartet werden kann.

Sollten die beiden Commands nicht funktionieren, so kann man die Teilnehmer die Projektes anfragen.

Die Applikation startet nun eine Rest-API, welche die auf dem Port 8081 auf localhost läuft. Dies stellt mehrere Endpoints zur Verfügung:

/addTransaction (POST)

Body-Parameter:

fromAddress,
toAddress,
amount

/mine (POST)

Body-Parameter:

address


/address/{address}

Parameter "address" ist hierbei die Adresse des gewollten Users. Damit kann der Stand des Kontos überprüft werden.