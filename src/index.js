const express = require('express');
const app = express();
const Blockchain = require('./Blockchain/Blockchain');
const Transaction = require('./Blockchain/Transaction');
const bodyParser = require('body-parser');

let blockChain;

app.use(bodyParser.json());

app.post("/addTransaction", function (req, res) {
    blockChain.createTransaction(
        new Transaction(
            req.body.fromAddress,
            req.body.toAddress,
            req.body.amount,
            Date.now()
        )
    );
    res.send();
});

app.post("/mine", function (req, res) {
    blockChain.minePendingTransactions(req.body.address);
    res.send(blockChain.getChain());
});

app.get("/address/:address", function (req, res) {
    console.log(blockChain.getBalanceOfAddress(req.params.address));
    res.send(blockChain.getBalanceOfAddress(req.params.address));
});

const server = app.listen(3000, function () {
    blockChain = new Blockchain();
    console.log('Blockchain initialized and ready to go');
});
